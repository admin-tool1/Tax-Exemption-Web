import React, { Component } from "react";
import { CCol, CRow } from "@coreui/react";
import Term from "../TaxExemption/TermData";
import RequestButton from "../TaxExemption/RequestButton";

class TermPage extends Component {
  render() {
    const ColoredLine = ({ color }) => (
      <hr
        style={{
          color: color,
          backgroundColor: color,
          height: 1,
        }}
      />
    );

    return (
      <div className="containter">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            {}
            <Term />
            <ColoredLine color="info" />
            <RequestButton />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TermPage;
