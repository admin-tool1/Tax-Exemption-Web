import React from "react";
import { CButton } from "@coreui/react";

const RequestButton = () => {
  return (
    <CButton
      size="md"
      className="requestTaxExemptionButton mx-2 my-2"
      color="success"
      variant="outline"
      to="/request-tax-exemption"
    >
      Request Tax Exemption
    </CButton>
  );
};

export default RequestButton;
