import React from "react";

const Faqs = [
  { id: 1, Question: "Question 1", Answer: "Answer 1" },
  { id: 2, Question: "Question 2", Answer: "Answer 2" },
  { id: 3, Question: "Question 3", Answer: "Answer 3" },
];

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 1,
    }}
  />
);

function Faq() {
  return (
    <div>
      <h4>Frequently Asked Questions</h4>
      <ColoredLine color="info" />

      {Faqs.map((Faq) => (
        <div>
          {" "}
          <p>
            <strong>{Faq.Question}</strong>
          </p>
          <p className="text-muted">{Faq.Answer}</p>
        </div>
      ))}
    </div>
  );
}

export default Faq;
