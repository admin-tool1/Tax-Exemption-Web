import React from "react";

const Terms = [
  {
    id: 1,
    No: 1,
    Term: "You must include the receipt of the donation transaction",
  },
  {
    id: 2,
    No: 2,
    Term: "Contributions eligible for tax exemption are RM 500 and above",
  },
  {
    id: 3,
    No: 3,
    Term: "Receipts will be provided via email, while postage is based on your request and charges RM 5.00",
  },
  {
    id: 4,
    No: 4,
    Term: "The tax exemption receipt process takes more than a month.",
  },
];

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 1,
    }}
  />
);

function Term() {
  return (
    <div>
      <h4>Terms & Conditions</h4>
      <ColoredLine color="info" />

      {Terms.map((Term) => (
        <div>
          {" "}
          <p key={Term.id}>
            <strong>{Term.No}</strong> {Term.Term}
          </p>
        </div>
      ))}
    </div>
  );
}

export default Term;
