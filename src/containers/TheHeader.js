import React from "react";
import { CJumbotron } from "@coreui/react";

function TheHeader() {
  return (
    <div>
      <CJumbotron
        style={{
          background: "linear-gradient(to bottom, #228541 5%, #237F03 100%)",
        }}
      >
        <h1 className="text-center" style={{ color: "white" }}>
          TAX EXEMPTION
        </h1>
      </CJumbotron>
    </div>
  );
}
export default TheHeader;
