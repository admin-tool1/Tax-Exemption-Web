import React from "react";
import { CFooter } from "@coreui/react";

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div className="mfs-auto">
        <a
          href="https://cintasyriamalaysia.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Cinta Syria Malaysia
        </a>
        <span className="ml-1">&copy; 2021 Partnership with NJ and AL</span>
      </div>
    </CFooter>
  );
};

export default React.memo(TheFooter);
