import React, { Component } from "react";
import { CCol, CRow } from "@coreui/react";
import Faq from "../TaxExemption/FaqData";
import RequestButton from "../TaxExemption/RequestButton";

class TaxRequestForm extends Component {
  render() {
    const ColoredLine = ({ color }) => (
      <hr
        style={{
          color: color,
          backgroundColor: color,
          height: 1,
        }}
      />
    );

    return (
      <div className="containter">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            {" "}
            <Faq />
            <ColoredLine color="info" />
            <RequestButton />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TaxRequestForm;
