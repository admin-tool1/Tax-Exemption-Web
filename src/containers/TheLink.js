import { React, Component } from "react";
import { CLink } from "@coreui/react";

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 1,
    }}
  />
);

const LinkStyle = {
  textDecoration: "none",
  color: "black",
  fontSize: 15,
  fontWeight: 600,
};

class TheLink extends Component {
  constructor() {
    super();
    this.state = {
      hover: false,
    };

    this.toggleHover = this.toggleHover.bind(this);
    this.toggleOutHover = this.toggleOutHover.bind(this);
  }

  toggleHover(e) {
    e.target.style.color = "#206209";
  }

  toggleOutHover(e) {
    e.target.style.color = "black";
  }

  render() {
    return (
      <div>
        <CLink
          to="tax-exemption"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          Frequently Asked Questions
        </CLink>
        <ColoredLine color="info" />
        <CLink
          to="/term-tax-exemption"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          Terms & Conditions
        </CLink>

        <ColoredLine color="info" />
        <CLink
          to="/request-tax-exemption"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          Request Tax Exemption
        </CLink>
        <ColoredLine color="info" />
        <CLink
          to="/tracking"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          Track Tax Exemption
        </CLink>
        <ColoredLine color="info" />
      </div>
    );
  }
}
export default TheLink;
